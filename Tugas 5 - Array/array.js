function enter (){
    console.log("\n");
}

/*
Soal No. 1 (Range)
Buatlah sebuah function dengan nama range() yang menerima dua parameter berupa number. 
Function mengembalikan sebuah array yang berisi angka-angka mulai dari angka parameter pertama hingga angka pada parameter kedua. 
Jika parameter pertama lebih besar dibandingkan parameter kedua maka angka-angka tersusun secara menurun (descending).
*/

function range(startNum = null, endNum = null){
    var range = [];
    if (startNum > endNum){
        for (var i = startNum; i >= endNum; i--){
            range.push(i);
        };
        console.log(range);
    }
    else if (startNum < endNum) {
        for (var i = startNum; i <= endNum; i++){
            range.push(i);
        };
        console.log(range);
    }
    else console.log ("-1");
};

range (10, 20);
enter();

/*
Soal No.2 (Range With Step)
Pada soal kali ini kamu diminta membuat function rangeWithStep yang mirip dengan function range di soal sebelumnya,
namun parameternya ditambah dengan parameter ketiga yaitu angka step yang menyatakan selisih atau beda dari setiap angka pada array. 
Jika parameter pertama lebih besar dibandingkan parameter kedua maka angka-angka tersusun secara menurun (descending) dengan step sebesar parameter ketiga.
*/