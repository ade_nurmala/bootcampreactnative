/*
Soal No.2 (Range With Step)
Pada soal kali ini kamu diminta membuat function rangeWithStep yang mirip dengan function range di soal sebelumnya,
namun parameternya ditambah dengan parameter ketiga yaitu angka step yang menyatakan selisih atau beda dari setiap angka pada array. 
Jika parameter pertama lebih besar dibandingkan parameter kedua maka angka-angka tersusun secara menurun (descending) dengan step sebesar parameter ketiga.
*/

function rangeWithStep(startNum, endNum, step = 1 ){
    var range = [];
    if (startNum > endNum){
        for (var i = startNum; i >= endNum; i - step){
            range.push(i);
        };
        console.log(range);
    }
    else if (startNum < endNum) {
        for (var i = startNum; i <= endNum; i + step){
            range.push(i);
        };
        console.log(range);
    }
    else console.log ("-1");
};

rangeWithStep (10, 20, 2);