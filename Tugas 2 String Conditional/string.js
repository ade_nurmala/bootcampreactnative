// Soal No.1 Membuat Kalimat
var word = 'JavaScript';
var second = 'is';
var third = 'awesome';
var fourth = 'and';
var fifth = 'I';
var sixth = 'love';
var seventh = 'it!';

console.log(word + ' ' + second + ' '+ third + ' ' + 
            fourth+ ' '+ fifth + ' ' + sixth + ' '+ seventh);

//Soal No.2 Mengurai kalimat (Akses karakter dalam string)
var sentence = 'I am going to be React Native Developer'

var firstWord = sentence [0];
var secondWord = sentence.substr(2, 2);
var thirdWord = sentence.substr(5,5);
var fourthWord = sentence.substr(11, 2);
var fifthWord = sentence.substr(14,2);
var sixthWord = sentence.substr(17, 5);
var seventhWord = sentence.substr(23, 6);
var eighthWord = sentence.substr(30);

console.log('First Word: ' + firstWord); 
console.log('Second Word: ' + secondWord); 
console.log('Third Word: ' + thirdWord); 
console.log('Fourth Word: ' + fourthWord); 
console.log('Fifth Word: ' + fifthWord); 
console.log('Sixth Word: ' + sixthWord); 
console.log('Seventh Word: ' + seventhWord); 
console.log('Eighth Word: ' + eighthWord);

//Soal No.3 Mengurai kalimat (Substring)
var sentence2 = 'wow JavaScript is so cool';
var firstWord2 = sentence2.substring(0, 3);
var secondWord2 = sentence2.substring(4, 14);
var thirdWord2 = sentence2.substring(15, 17);
var fourthWord2 = sentence2.substring(18, 20);
var fifthWord2 = sentence2.substring(21);

console.log('First Word: ' + firstWord2); 
console.log('Second Word: ' + secondWord2); 
console.log('Third Word: ' + thirdWord2); 
console.log('Fourth Word: ' + fourthWord2); 
console.log('Fifth Word: ' + fifthWord2);

//Soal No.4 Mengurai Kalimat dan Menentukan Panjang String
var sentence3 = 'wow JavaScript is so cool';
var firstWord3 = sentence3.substring(0, 3);
var secondWord3 = sentence3.substring(4, 14);
var thirdWord3 = sentence3.substring(15, 17);
var fourthWord3 = sentence3.substring(18, 20);
var fifthWord3 = sentence3.substring(21);

console.log('First Word: ' + firstWord3 + ', with length: ' + firstWord3.length); 
console.log('Second Word: ' + secondWord3 + ', with length: ' + secondWord3.length);
console.log('Third Word: ' + thirdWord3 + ', with length: ' + thirdWord3.length); 
console.log('Fourth Word: ' + fourthWord3 + ', with length: ' + fourthWord3.length); 
console.log('Fifth Word: ' + fifthWord3 + ', with length: ' + fifthWord3.length); 