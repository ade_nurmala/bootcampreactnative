/*
If-else

Petunjuk : Kita akan memasuki dunia game werewolf. 
Pada saat akan bermain kamu diminta memasukkan nama dan peran . 
Untuk memulai game pemain harus memasukkan variable nama dan peran. 
Jika pemain tidak memasukkan nama maka game akan mengeluarkan warning “Nama harus diisi!“. 
Jika pemain memasukkan nama tapi tidak memasukkan peran maka game akan mengeluarkan warning “Pilih Peranmu untuk memulai game“. 
Terdapat tiga peran yaitu penyihir, guard, dan werewolf. 
Tugas kamu adalah membuat program untuk mengecek input dari pemain dan hasil response dari game sesuai input yang dikirimkan.
*/

var nama ;
var peran;

function greeting(nama){
    console.log ("Selamat datang di Dunia Warewolf, " + nama);
}

if (nama != null && peran != null){
    if (peran == "penyihir"){
        greeting(nama)
        console.log ("Hello penyihir " + nama +
                    ", kamu dapat melihat siapa yang menjadi warewolf!");
    }else if (peran == "guard"){
        greeting(nama)
        console.log ("Hello guard " + nama +
                     ", kamu dapat melindungi temanmu dari serangan warewolf!");
    }else if (peran == "warewolf"){
        greeting(nama)
        console.log ("Hello warewolf " + nama +
                     ", kamu dapat mangsa setiap malam!");
    }else{
        console.log ("Hai "+ nama + " Pilih peranmu untuk memulai game!");
    } 
}else{
    console.log("Nama harus diisi!");
}

/*
Switch Case
Kamu akan diberikan sebuah tanggal dalam tiga variabel, yaitu hari, bulan, dan tahun. 
Disini kamu diminta untuk membuat format tanggal. 
Misal tanggal yang diberikan adalah hari 1, bulan 5, dan tahun 1945. 
Maka, output yang harus kamu proses adalah menjadi 1 Mei 1945.
*/

var hari = 22; 
var bulan = 1; 
var tahun = 2000;

function warning(){
    console.log("Masukkan tanggal yang tepat!")
}

if(hari > 0 && hari < 32){
    if(tahun>1899 && tahun < 2201){
        if (bulan > 0 && bulan < 13){
            switch (bulan){
                case 1: {bulan = "Januari"; break;}
                case 2: {bulan = "Februari"; break;}
                case 3: {bulan = "Maret"; break;}
                case 4: {bulan = "April"; break;}
                case 5: {bulan = "Mei"; break;}
                case 6: {bulan = "Juni"; break;}
                case 7: {bulan = "Juli"; break;}
                case 8: {bulan = "Agustus"; break;}
                case 9: {bulan = "September"; break;}
                case 10: {bulan = "Oktober"; break;}
                case 11: {bulan = "November"; break;}
                case 12: {bulan = "Desember"; break;}
            }
            console.log (hari + ' ' + bulan + ' ' + tahun);
        }else {
            warning();
            
        }
    
    }else warning();
}else warning();


