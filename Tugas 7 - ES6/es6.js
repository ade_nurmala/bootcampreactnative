function separator (nomor){
    console.log(`======= ${nomor} =======`)
};

// No. 1
separator(1);

golden = () => {
    console.log("This is Golden!");
};

golden();

// No. 6
separator(6);

const planet = "earth";
const view = "glass";

let after = `Lorem ${view} dolor sit amet, consectetur adipiscing elit, ${planet} do siusmod tempor incididunt ut labore et dolore magna aliqua. ut enim ad minim veniam`;

console.log(after);
