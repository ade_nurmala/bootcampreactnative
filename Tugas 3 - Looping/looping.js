function enter(){
    console.log("\n")
}

/*
No. 1 Looping While
Pada tugas ini kamu diminta untuk melakukan looping dalam JavaScript dengan menggunakan syntax while. 
Untuk membuat tantangan ini lebih menarik, 
kamu juga diminta untuk membuat suatu looping yang menghitung maju dan menghitung mundur. 
Jangan lupa tampilkan di console juga judul ‘LOOPING PERTAMA’ dan ‘LOOPING KEDUA’.”
*/

//Looping Pertama
console.log("LOOPING PERTAMA");
var j = 2;

while (j < 21 && j > 1){
    console.log (j + " - I Love Coding");
    j+=2;
}
enter()

// //Looping Kedua
console.log("LOOPING Kedua");

var i = 20;

while (i < 21 && i > 1){
    console.log (i + " - I Will Become a Mobile Developer");
    i-=2;
}
enter()

/*
No. 2 Looping Menggunakan For
Pada tugas ini kamu diminta untuk melakukan looping dalam JavaScript dengan menggunakan syntax for. 
Untuk membuat tantangan ini lebih menarik, kamu juga diminta untuk memenuhi syarat tertentu yaitu:

SYARAT:
A. Jika angka ganjil maka tampilkan Santai
B. Jika angka genap maka tampilkan Berkualitas
C. Jika angka yang sedang ditampilkan adalah kelipatan 3 DAN angka ganjil maka tampilkan I Love Coding.
*/

for (var a = 1; a < 21; a++){
    if (a % 2 == 0){
        console.log(a + " - Berkualitas")
    } else if( a % 3 == 0) {
        console.log(a + " - I Love Coding")
    } else{
        console.log(a + " - Santai")
    }
} 
enter()

/*
No.3 Membuat Persegi Panjang #
Kamu diminta untuk menampilkan persegi dengan dimensi 8×4 dengan tanda pagar (#) dengan perulangan atau looping.
Looping boleh menggunakan syntax apa pun (while, for, do while).
*/

for (var b = 1; b < 5; b++){
    var hashtag = " ";
    for (var c = 8; c > 0; c--){
        hashtag = hashtag + "#" ;
    } console.log(hashtag);
}
enter()

/*
No. 4 
Kali ini kamu diminta untuk menampilkan sebuah segitiga dengan tanda pagar (#) dengan dimensi tinggi 7 dan alas 7. 
Looping boleh menggunakan syntax apa pun (while, for, do while).
*/

for (var b = 1; b < 8; b++){
    var hashtag = " ";
    for (var c = b; c > 0; c--){
        hashtag = hashtag + "#" ;
    } console.log(hashtag);
}
enter()

/*
No. 5 Membuat Papan Catur
Buatlah suatu looping untuk menghasilkan sebuah papan catur dengan ukuran 8 x 8 . 
Papan berwarna hitam memakai tanda pagar (#) sedangkan papan putih menggunakan spasi. 
Looping boleh menggunakan syntax apa pun (while, for, do while).
*/
var ukuran = 8;

for(i = ukuran; i > 0; i--){
    var hashtag = ""
    if (i % 2 == 0){
        for (j = ukuran/2; j > 0; j--){
            hashtag = hashtag + " #"
        }console.log(hashtag);
    }else {
        for (j = ukuran/2; j > 0; j--){
            hashtag = hashtag + "# "
        }console.log(hashtag);
    }
}
enter()