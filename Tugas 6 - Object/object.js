/*
Soal No. 1 (Range)
Buatlah function dengan nama arrayToObject() yang menerima sebuah parameter berupa array multidimensi. 
Dalam array tersebut berisi value berupa First Name, Last Name, Gender, dan Birthyear. 
Data di dalam array dimensi tersebut ingin kita ubah ke dalam bentuk Object dengan key bernama : firstName, lastName, gender, dan age. 
Untuk key age ambillah selisih tahun yang ditulis di data dengan tahun sekarang. 
Jika tahun tidak terdefinisi atau tahunnya lebih besar dibandingkan dengan tahun sekarang maka kembalikan nilai : “Invalid birth year”.

Contoh: jika input nya adalah [["Abduh", "Muhamad", "male", 1992], ["Ahmad", "Taufik", "male", 1985]]

maka outputnya di console seperti berikut :

1. Abduh Muhamad : { firstName: "Abduh", lastName: "Muhamad", gender: "male", age: 28}
2. Ahmad Taufik : { firstName: "Ahmad", lastName: "Taufik", gender: "male", age: 35} 
*/

function arrayToObject (arr){
    var now = new Date();
    var yearNow = now.getFullYear();

    var result = [];
    for (var i = 0; i < arr.length ; i++){
        var temp = {
            firstName: arr[i][0],
            lastName: arr[i][1],
            gender: arr[i][2],
            age: yearNow - arr[i][3],
            }

        var res = i + 1 + '. '+ arr[i][0] + ' ' + arr[i][1];
        var dataTemp = res + ':' + temp;
        result.push(dataTemp);
    }
    return result;
 }

 var people = [ 
     ["Bruce", "Banner", "male", 1975], 
     ["Natasha", "Romanoff", "female"] 
    ];

 console.log(arrayToObject(people));


 /*
 Soal No. 2 (Shopping Time)
 */

 function shoppingTime (memberId, money){

 }